﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wisielec
{
    class Program
    {
        static void Main(string[] args)
        {
            //************ FINISHED VAR **************
            // create finished - bool var indicating if game is finished: 1) player guessed all characters, 2) whole hangman is printed
            Boolean finished = false;

            //************ INTRO STRING **************
            // create intro string
            String intro = "Witaj w grze Wisielec. Komputer wygeneruje slowo.\nTwoim zadaniem jest odgadniecie tego slowa, po kolei zgadujac litery, ktore sie na nie skladaja.\nZa kazda pomylke komputer narysuje fragment szubienicy z wisielcem.\nGra konczy sie zwyciestwem, jesli odgadniesz wszystkie litery.\nGra konczy sie przegrana, jesli komputer narysuje cala szubienice. Powodzenia!";

            //************ ARRAY OF WORDS **************
            // create string Array with word
            string[] wordList = { "wrzeciono", "posmak", "kikut", "masakracja", "dziąsło", "przeor", "dżdżownica", "śpiulkolot" };


            //************ HANGMAN **************
            // stupid way but should work
            // doesn't accept \
            string hangmanBase = "| |";
            string hangmanPipe = " |";
            string hangmanPipeSlash = " |/";
            string hangmanUnderscores = " _____";
            string hangmanPipeSlashPipe = " |/   |";
            string hangmanPipeO = " |    O";
            string hangmanPipeHyphenPipeHyphen = " |   -|-";
            string hangmanPipePipePipe = " |   | |";


            // test print whole hangman
            Console.WriteLine(hangmanUnderscores);
            Console.WriteLine(hangmanPipeSlashPipe);
            Console.WriteLine(hangmanPipeO);
            Console.WriteLine(hangmanPipeHyphenPipeHyphen);
            Console.WriteLine(hangmanPipePipePipe);
            Console.WriteLine(hangmanBase);
            ;

            //************ INTRO **************
            Console.WriteLine(intro);
            Console.WriteLine("************");
            Console.WriteLine("");
            Console.WriteLine("Słowo do odgadniecia:");

            //************ RANDOM WORD **************
            // create new random
            Random random = new Random();
            // generate random word
            int indexOfWordInArray = random.Next(0, wordList.Length);
            string randomWord = wordList[indexOfWordInArray];
            // DEV: print randomWord
            Console.WriteLine($"word is:\t{randomWord}");


            //************ GUESSED WORD VAR **************
            // create empty string for guessed word
            string guessedWord = "";


            //************ ARRAY OF UNDERSCORES **************
            // create list of underscores equal to random word length
            List<string> underscoresList = new List<string>();
            // DEV: print list of underscores
            for (int i = 0; i < randomWord.Length; i++)
            {
                underscoresList.Add("_ ");
            }
            // convert list of underscores to array
            string[] underscoreArray = underscoresList.ToArray();

            //************ MAIN WHILE LOOP **************
            // runs until finished is not true
            while (!finished)
            {
                Console.WriteLine("");
                Console.WriteLine("");
                // ToDo: until hangman is printed or all "_" are replaced with letters
                while (true)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Zgadnij litere:");

                    // read letter
                    // ToDo: try-catch if user enters >1 letter or not a letter
                    string guessedLetter = Console.ReadLine().ToLower();
                    char guessedLetterChar = char.Parse(guessedLetter);


                    if (guessedLetter.Length > 1)
                    {
                        Console.WriteLine("Wpisz pojedyncza litere");
                    }


                    // check if input letter is in our word and replace "_" with it
                    if (wordList[indexOfWordInArray].Contains(guessedLetter))
                    {
                        Console.WriteLine($"word is now {guessedWord}");

                        // iterate through random word array and replace _ with guesses letter
                        for (int i = 0; i < wordList[indexOfWordInArray].Length; i++)
                        {
                            if (guessedLetterChar == wordList[indexOfWordInArray][i])
                            {
                                Console.WriteLine(guessedLetterChar + " ");
                                // replace element of index i in underscore array with guessed letter and space
                                underscoreArray[i] = guessedLetter + " ";
                                guessedWord = string.Join("", underscoreArray);
                                Console.WriteLine($"word is now {guessedWord}");
                                foreach (string e in underscoreArray)
                                {
                                    Console.WriteLine($"to jest underscorearray po zmianach {e}");
                                }
                            }
                            else
                            {
                                underscoreArray[i] = "_ ";
                                guessedWord = string.Join("", underscoreArray);
                                Console.WriteLine($"word is now {guessedWord}");
                                foreach (string e in underscoreArray)
                                {
                                    Console.WriteLine($"to jest underscorearray po zmianach {e}");
                                }

                            }
                        } // else print part of hangman


                    }


                    // finished = true;


                }

            }
        }

    }
}